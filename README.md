# Algoritmos #

Esse repositório contém o código da implementação dos **algoritmos** descritos no seguinte link: [https://mega.nz/#!6hE...](https://mega.nz/#!6hE0zQyK!79ub_sQPzjEa-31a0U4m6PRvhZc5DkbDGbiS0blPAvE)

### Setup para execução ###

1. Instale o Node (versão utilizada: 8.1.11).
2. Navege até a raíz do projeto.
3. Execute o comando `npm start`.

### Contato ###
Para mais informações acesse **[emilianooliveira.com](http://emilianooliveira.com)** ou entre em contato com **[emilianom.oliveira.n@gmail.com](mailto:emilianom.oliveira.n@gmail.com)**.