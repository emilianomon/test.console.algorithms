
module.exports = (firstStepSize) => {
    
    var i; // Avoids repeated declarations.
    var step; // Avoids repeated declarations.
    let ladder = '\n';
    function drawStep(currentStep=1) {
        step = '';
        for(i = firstStepSize; i > 0; i--) {
            step += i > currentStep ? ' ' : '#';
        }
        ladder += step + '\n';

        if(currentStep != firstStepSize)
            drawStep(currentStep+1);
    }
    
    drawStep();
    console.log(ladder.slice(0, -1)); // Removes last line break.
}