
module.exports = (matrix) => {
    
    const matrix_length = matrix.length-1; // Avoids repeated object access and operations.
    let i; // Avoids repeated declarations.
    let leftToRight = 0;
    let rightToLeft = 0;
    
    for(i = 0; i <= matrix_length; i++) {
        leftToRight += matrix[i][i];
        rightToLeft += matrix[matrix_length-i][i];
    }

    console.log('\nLeft to right: ' + leftToRight +
                '\nRight to left: ' + rightToLeft +
                '\nAbsolute difference: ' + Math.abs(leftToRight - rightToLeft));
}