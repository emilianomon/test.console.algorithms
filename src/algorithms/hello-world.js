
module.exports = () => {
    console.log('');

    let i; // Avoids repeated declarations.
    let message = ''; // Avoids repeated reclarations.
    for(i = 1; i < 101; i++) { // Initiating at 1 avoids unecessary operations.
        message += ((i % 3 == 0 ? 'Hello ' : '') + (i % 7 == 0 ? 'World' : '') || i) + '\n';
    }
    console.log(message.slice(0, -1)); // Removes last line break.
}

