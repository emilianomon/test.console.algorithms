const readLine = require('readline');
const fs = require('fs');
const ladder = require('./algorithms/ladder');
const helloWorld = require('./algorithms/hello-world');
const matrix = require('./algorithms/matrix');


// ----------------------------------------
const CONSTANTS = JSON.parse(fs.readFileSync(__dirname + '/resources/constants.json'));
let readLineInterface;
init();
// ----------------------------------------


function init() {
    readLineInterface = readLine.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    askForAlgorithm();
}

function askForAlgorithm() {
    readLineInterface.question(CONSTANTS.ALGORITHMS_QUESTION, (choice) => {
        switch (parseInt(choice)) {
            case 1: askForLadderSize(); break;
            case 2: callHelloWorld(); break;
            case 3: askForMatrix(); break;
            case 0: readLineInterface.close(); break;
            default: 
                console.log('Invalid option.');
                askForAlgorithm();
                break;
        }
    });
};

function askForLadderSize() {
    readLineInterface.question(CONSTANTS.LADDER_QUESTION, (ladderSize) => {
        ladder(parseInt(ladderSize));
        askForAlgorithm();
    });
}

function callHelloWorld() {
    helloWorld();
    askForAlgorithm();
}

function askForMatrix() {
    readLineInterface.question(CONSTANTS.MATRIX_QUESTION + CONSTANTS.MATRIX_OPTIONS, (stringMatrix) => {
        matrix(JSON.parse(stringMatrix));
        askForAlgorithm();
    });
}
